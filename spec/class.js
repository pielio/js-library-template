import { assert } from 'chai';
import cls from '../src';

describe( 'Tests the library class', () => {

	var Instance;

	it( 'Instantiates the class' , () => {

		Instance = new cls();
		assert( Instance instanceof cls, 'Class could not be instantiated' );
	})

});
